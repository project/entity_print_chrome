<?php

namespace Drupal\entity_print_chrome\EventSubscriber;

use Drupal\entity_print\Event\PrintHtmlAlterEvent;
use Drupal\entity_print\EventSubscriber\PostRenderSubscriber as BasePostRenderSubscriber;
use Masterminds\HTML5;

/**
 * The PostRenderSubscriber class.
 */
class PostRenderSubscriber extends BasePostRenderSubscriber {

  /**
   * Rewrites relative paths to file:/// URLs.
   *
   * @param \Drupal\entity_print\Event\PrintHtmlAlterEvent $event
   *   The event object.
   *
   * @see https://drupal.org/node/1494670
   */
  public function postRender(PrintHtmlAlterEvent $event) {
    // Only for the Chrome rendering engine.
    $config = $this->configFactory->get('entity_print.settings');
    if ($config->get('print_engines.pdf_engine') !== 'chrome') {
      return;
    }

    // Do not rewrite URLs in debug mode.
    if (in_array(
      \Drupal::routeMatch()->getRouteName(),
      ['entity_print.view.debug', 'entity_print_views.view.debug']
    )) {
      return;
    }

    $html_string = &$event->getHtml();
    $html5 = new HTML5();
    $document = $html5->loadHTML($html_string);

    // Define a function that will convert root relative uris into absolute
    // urls.
    $transform = function ($tag, $attribute) use ($document) {
      $base_url = 'file://' . getcwd();
      foreach ($document->getElementsByTagName($tag) as $node) {
        $attribute_value = $node->getAttribute($attribute);

        // Handle protocol agnostic URLs as well.
        if (mb_substr($attribute_value, 0, 2) === '//') {
          $node->setAttribute($attribute, $base_url . mb_substr($attribute_value, 1));
        }
        elseif (mb_substr($attribute_value, 0, 1) === '/') {
          $node->setAttribute($attribute, $base_url . $attribute_value);
        }
      }
    };

    // Transform stylesheets, links and images.
    $transform('link', 'href');
    $transform('a', 'href');
    $transform('img', 'src');

    // Overwrite the HTML.
    $html_string = $html5->saveHTML($document);
  }

}
